
<!-- pagebreak -->
# Kinesis Test Helper Module
This module lets you create test data for kinesis consumers ( currently i mostly use usable for insights )

##Usage

The lambda endpoint is at `http://kinesis-test-helper.qa.dolap.local/api/send`

Post your test data with body:
```
(ie, this is for boost insight)
{
    "stream": "qa-boost-insights-stream",
    "count": 100,
    "skeleton": {
        "id": "1.incrementNum",
        "item_id": "1.incrementNum",
        "product_id": "1.incrementNum",
        "like_count": "20.randomNum",
        "click_count": "40.incrementNum",
        "impression_count": "60.incrementNum", 
        "calculation_time": "2021-10-19T14:38:32.025Z"
    }
}
```

stream field indicates the stream you want to send your data,
count field indicates amount of data to be sent.
Currently you cant pass number fields as string and add a special identifier to alter data in batch requests,
There are 3 available currently:

`.incrementNum`: pass your number as string json and append this identifier to make it incremented in each request for count

`.randomNum`: pass your number as string json and append this identifier to make it randomized with some salt in each request for count

`.randomString`: append this identifier to string to append some random parts to your strinds: Such "username": "john .randomString" will send requests with random "john qwezScWs"

If randomization is not needed, you should pass your fields as intended as json, ie pass "like_count": 20 instead of "like_count": "20" if your consumer uses float. Randomizer/Incrementers convert the data in runtime if you provide the special identifiers.