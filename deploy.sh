#!/bin/bash

cd iac || exit

if [ "$1" == diff ]; then
  cdk diff kinesis-test-helper-${ENVIRONMENT}
elif [ "$1" == deploy ]; then
  cdk deploy --require-approval never kinesis-test-helper-${ENVIRONMENT}
elif [ "$1" == destroy ]; then
  echo y | cdk destroy kinesis-test-helper-${ENVIRONMENT}
fi
