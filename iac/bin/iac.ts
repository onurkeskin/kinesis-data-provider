#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { LambdaStack } from '../lib/stacks/KinesisTestHelperLambdas';
import { KinesisTestHelperEnvironment } from "../lib/stacks/types/KinesisTestHelperEnvironment";
import { KinesisTestHelperStackProps } from "../lib/stacks/types/KinesisTestHelperProps";
import { getVpc } from "../lib/stacks/vpc"

const app = new cdk.App();

const env = {
  account: '839739552976',
  region: 'eu-central-1'
}

const environments: KinesisTestHelperStackProps[] = [
  {
    environment: KinesisTestHelperEnvironment.QA,
    env
  },
  {
    environment: KinesisTestHelperEnvironment.PROD,
    env
  }
]

environments.forEach(props => {
  const environment = props.environment;
  new LambdaStack(app, "kinesis-test-helper-" + environment, {
    ...props, tags: {
      Stage: environment,
      Project: 'Kinesis-Test-Helper',
      Domain: 'Insights',
      ManagedBy: 'CDK-CF',
      Team: 'Seller and Operations',
    }
  });
})
