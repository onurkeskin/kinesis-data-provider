import * as lambda from '@aws-cdk/aws-lambda'
import { CfnOutput, Construct, Duration, Stack } from '@aws-cdk/core'
import { Code, Function } from '@aws-cdk/aws-lambda'
import { KinesisTestHelperStackProps } from './types/KinesisTestHelperProps'
import { Repository } from '@aws-cdk/aws-ecr'
import { IVpc } from "@aws-cdk/aws-ec2";
import { getVpc } from './vpc'

import { createCNameForALB } from "./route53";
import { Peer, Port, SecurityGroup } from "@aws-cdk/aws-ec2";
import { Effect, PolicyDocument, PolicyStatement } from "@aws-cdk/aws-iam"
import { DOLAP_INTERNAL_IP_RANGE, TRENDYOL_VPN_IP_RANGE } from "../constants";
import * as apigateway from '@aws-cdk/aws-apigateway';

export class LambdaStack extends Stack {
    public lambda: Function
    public vpc: IVpc

    constructor(scope: Construct, id: string, props: KinesisTestHelperStackProps) {
        super(scope, id, props)
        const commitId = process.env.CI_COMMIT_SHORT_SHA
        const lbPort = 80;

        let vpc = getVpc(this, props.environment)
        this.vpc = vpc

        const apiResourcePolicy = new PolicyDocument({
            statements: [
                new PolicyStatement({
                    effect: Effect.ALLOW,
                    actions: ['s3:Put*'],
                    resources: [bucket.bucketName],
                    principals: [new ServicePrincipal('apigateway.amazonaws.com')]
                })
            ]
        });

        const api = new apigateway.RestApi(this, 'kinesis-test-helper-api', {
            description: 'kinesis test helper api',
            deployOptions: {
                stageName: props?.environment,
            },
            // 👇 enable CORS
            defaultCorsPreflightOptions: {
                allowHeaders: [
                    'Content-Type',
                    'X-Amz-Date',
                    'Authorization',
                    'X-Api-Key',
                ],
                allowMethods: ['OPTIONS', 'GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
                allowCredentials: true,
                allowOrigins: ['http://localhost:3000'],
            },
            policy: {
                addStatements()
            }
        });

        new CfnOutput(this, 'apiUrl', { value: api.url });

        const ecrRepo = Repository.fromRepositoryName(this, "kinesis-test-helper", "kinesis-test-helper-" + props?.environment)
        const ecrImageCode = Code.fromEcrImage(ecrRepo, { tag: commitId })

        this.lambda = new lambda.Function(this, 'Lambda', {
            functionName: "kinesis-test-helper-" + props?.environment,
            memorySize: 128,
            handler: lambda.Handler.FROM_IMAGE,
            runtime: lambda.Runtime.FROM_IMAGE,
            code: ecrImageCode,
            timeout: Duration.seconds(6),
            environment: {},
            vpc: vpc,
        })

        const methods = api.root.addResource('api');
        const sendKinesis = methods.addResource('send')
        sendKinesis.addMethod(
            'POST',
            new apigateway.LambdaIntegration(this.lambda, { proxy: true }),
        )
        const statement = new PolicyStatement();
        statement.addActions('kinesis:*', 'lambda:*');
        statement.addResources(("*"));
        this.lambda.addToRolePolicy(statement)
    }
}