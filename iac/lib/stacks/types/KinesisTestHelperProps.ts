import { StackProps } from "@aws-cdk/core";
import { IVpc } from '@aws-cdk/aws-ec2'
import { KinesisTestHelperEnvironment } from './KinesisTestHelperEnvironment'


export interface KinesisTestHelperStackProps extends StackProps {
    environment: KinesisTestHelperEnvironment
}