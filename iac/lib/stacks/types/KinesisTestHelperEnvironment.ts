import * as cdk from '@aws-cdk/core';
import { Vpc } from '@aws-cdk/aws-ec2'

export enum KinesisTestHelperEnvironment {
    QA = "qa",
    PROD = "prod"
}


