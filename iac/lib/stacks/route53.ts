import { Construct } from "@aws-cdk/core";
import { KinesisTestHelperEnvironment } from "./types/KinesisTestHelperEnvironment";
import { CnameRecord, HostedZone } from "@aws-cdk/aws-route53";
import { API_HOST_PREFIX } from "../constants";
import { ApplicationLoadBalancer } from "@aws-cdk/aws-elasticloadbalancingv2";

export const createCNameForALB = (scope: Construct, environment: KinesisTestHelperEnvironment, alb: ApplicationLoadBalancer) => {
    const zone = HostedZone.fromLookup(scope, "kinesis-test-helper-route53-zone-" + environment, {
        domainName: environment + ".dolap.local",
        privateZone: true
    });

    new CnameRecord(scope, "kinesis-test-helper-route53-alb-cname-" + environment, {
        zone: zone,
        recordName: API_HOST_PREFIX,
        domainName: alb.loadBalancerDnsName
    });
}
