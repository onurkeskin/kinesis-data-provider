import { Vpc } from "@aws-cdk/aws-ec2";
import { Construct } from "@aws-cdk/core";
import { KinesisTestHelperEnvironment } from "./types/KinesisTestHelperEnvironment";

export const getVpc = (scope: Construct, environment: KinesisTestHelperEnvironment) => {
    const vpcName = "dolap-" + environment + "-vpc"
    return Vpc.fromLookup(scope, "dolap-vpc-" + environment, {
        vpcName
    });
}
