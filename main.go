package main

import (
	"context"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	env "github.com/caarlos0/env/v6"
	flags "github.com/jessevdk/go-flags"
	log "github.com/sirupsen/logrus"
	sendkinesisevent "gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/server"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/server/middlewares/renderer"

	"github.com/awslabs/aws-lambda-go-api-proxy/gorillamux"
)

var gorillaLambda *gorillamux.GorillaMuxAdapter
var commandLineArgs struct {
	Verbose []bool `short:"v" long:"verbose" description:"Show verbose debug information"`
}

type environmentConfiguration struct {
	AwsRegion string `env:"AWS_REGION"`
}

func init() {
	_, err := flags.Parse(&commandLineArgs)
	if err != nil {
		panic(err)
	}

	if len(commandLineArgs.Verbose) > 0 {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.ErrorLevel)
	}
	log.Printf("Log Level Set To: %s\n", log.GetLevel().String())

	cfg := environmentConfiguration{}
	if err := env.Parse(&cfg); err != nil {
		log.Printf("Couldn't load configs, %s\n", err.Error())
	}

	r := server.NewRouter()

	renderer := renderer.New(&renderer.Options{
		IndentJSON: true,
	}, renderer.JSON)

	sendKinesisEventRes := sendkinesisevent.NewResource(&sendkinesisevent.Options{
		Renderer: renderer,
		Service:  sendkinesisevent.NewSendKinesisEventService(cfg.AwsRegion),
	})

	r.AddResources(sendKinesisEventRes)

	gorillaLambda = gorillamux.New(r.Router)
}

func Handler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	return gorillaLambda.ProxyWithContext(ctx, req)
}

func main() {
	lambda.Start(Handler)
}
