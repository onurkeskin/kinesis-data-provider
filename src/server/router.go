package server

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/server/domain"
)

type Router struct {
	*mux.Router
}

func matcherFunc(r domain.Route, defaultHandler http.HandlerFunc) func(r *http.Request, rm *mux.RouteMatch) bool {
	return func(req *http.Request, rm *mux.RouteMatch) bool {
		acceptHeaders := domain.NewAcceptHeadersFromString(req.Header.Get("accept"))
		foundHandler := defaultHandler
		for _, h := range acceptHeaders {
			m := h.MediaType
			if !(m.Type == "application" && (m.SubType == "json" || m.Suffix == "json")) {
				continue
			}

			version, hasVersion := m.Parameters["version"]
			if !hasVersion {
				continue
			}
			if handler, ok := r.RouteHandlers[domain.RouteHandlerVersion(version)]; ok {
				foundHandler = http.HandlerFunc(injectMiddlewares(handler))
				// foundHandler = handler
				break
			}
		}

		rm.Handler = foundHandler

		return true
	}
}

func NewRouter() *Router {
	router := mux.NewRouter().StrictSlash(true)

	return &Router{router}
}

func (router *Router) AddRoutes(routes *domain.Routes) *Router {
	if routes == nil {
		return router
	}
	for _, route := range *routes {
		defaultHandler, ok := route.RouteHandlers[route.DefaultVersion]
		if !ok {
			panic(errors.New(fmt.Sprintf("Routes definition error, missing default route handler for version `%v` in `%v`",
				route.DefaultVersion, route.Name)))
		}
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Queries(route.Queries...).
			MatcherFunc(matcherFunc(route, http.HandlerFunc(injectMiddlewares(defaultHandler))))
	}
	return router
}

func (router *Router) AddResources(resources ...domain.IResource) *Router {
	for _, resource := range resources {
		if resource.Routes() == nil {
			// its safe to throw panic here
			panic(errors.New(fmt.Sprintf("Routes definition missing: %v", resource)))
		}
		router.AddRoutes(resource.Routes())
	}
	return router
}

func injectMiddlewares(f domain.HandlerFunc) domain.MiddlewareHandlerFunc {
	chain := domain.MiddlewareHandlerFunc(f.Controller)

	for _, mid := range f.Middleware {
		chain = chain.
			Intercept(mid)
	}

	return chain
}
