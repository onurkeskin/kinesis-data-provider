package sendkinesisevent

import (
	"net/http"

	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/server/domain"
)

type Options struct {
	BasePath string
	Renderer domain.IRenderer
	Service  EventSender
}

type Resource struct {
	options  *Options
	routes   *domain.Routes
	Renderer domain.IRenderer
	Service  EventSender
}

func NewResource(options *Options) *Resource {
	renderer := options.Renderer
	if renderer == nil {
		panic("sendkinesisevent.Options.Renderer is required")
	}

	resource := &Resource{
		options,
		nil,
		renderer,
		options.Service,
	}

	resource.generateRoutes(options.BasePath)
	return resource
}

func (resource *Resource) Routes() *domain.Routes {
	return resource.routes
}

func (resource *Resource) Render(w http.ResponseWriter, req *http.Request, status int, v interface{}) {
	resource.Renderer.Render(w, req, status, v)
}
