package model

import (
	"strconv"
	"strings"

	"github.com/Jeffail/gabs/v2"
	"github.com/sirupsen/logrus"
)

type ParsedSkeleton struct {
	ArrayIndexes []JsonArr
	Types        map[string]ValueFunc
}

type JsonArr struct {
	ArrayPath string
	Index     int
	Capacity  int
}

type ValueResolvingFunc func() ValueFunc
type ValueFunc func() interface{}

type DataSupplier interface {
	PrepareNext() (string, error)
}

func (f ValueResolvingFunc) GetValuePreparer() ValueFunc {
	return f()
}

func (parsedSkeleton *ParsedSkeleton) PrepareNext() (string, error) {
	jsonObj := gabs.New()

	for _, value := range parsedSkeleton.ArrayIndexes {
		if value.ArrayPath == "" {
			_, err := jsonObj.ArrayOfSize(value.Capacity)
			if err != nil {
				logrus.Info(err.Error())
			}
		} else {
			var target *gabs.Container
			if strings.LastIndex(value.ArrayPath, ".") == -1 {
				target = jsonObj
			} else {
				target = jsonObj.Path(value.ArrayPath[:strings.LastIndex(value.ArrayPath, ".")])
			}
			_, err := target.ArrayOfSizeI(value.Capacity, value.Index)
			if err != nil {
				target.ArrayOfSizeP(value.Capacity, value.ArrayPath)
			}
		}
	}

	for path, value := range parsedSkeleton.Types {
		lastDot := strings.LastIndex(path, ".")
		if lastDot == -1 { // Base
			if path == "" { // ROOT
				switch jsonObj.Data().(type) {
				case map[string]interface{}: // Object
					jsonObj.Set(value())
				case []interface{}: // Array
				}
			} else if pos, err := strconv.Atoi(path); err == nil { // Array
				jsonObj.SetIndex(value(), pos)
			} else { // Object
				jsonObj.SetP(value(), path)
			}
		} else {
			finalPart := path[lastDot+1:]
			secondFinalPart := path[:lastDot]
			otherFinalDot := strings.LastIndex(secondFinalPart, ".")
			var priorPart string
			var restPart string
			if otherFinalDot == -1 {
				priorPart = path[:lastDot]
				restPart = ""
			} else {
				priorPart = path[otherFinalDot+1 : lastDot]
				restPart = path[:otherFinalDot]
			}

			if pos, err := strconv.Atoi(finalPart); err == nil { // Array
				arrayToInsertInto := jsonObj.Path(secondFinalPart)
				arrayToInsertInto.SetIndex(value(), pos)
			} else {
				if isArray(priorPart) {
					var objectToInsertInto *gabs.Container
					if restPart == "" {
						objectToInsertInto = jsonObj
					} else {
						objectToInsertInto = jsonObj.Path(restPart)
					}
					check := jsonObj.Path(secondFinalPart).Data()
					var toPut map[string]interface{}
					if check == nil {
						toPut = make(map[string]interface{})
					} else {
						toPut = check.(map[string]interface{})
					}

					toPut[finalPart] = value()
					objectToInsertInto.SetIndex(toPut, asArrayIndex(priorPart))
				} else {
					// objectToInsertInto := jsonObj.Path(priorPart)
					jsonObj.SetP(value(), path)
				}
			}
		}
	}

	return jsonObj.String(), nil
}

type TextData string

func (textData TextData) PrepareNext() (string, error) {
	return string(textData), nil
}

func isArray(s string) bool {
	if _, err := strconv.Atoi(s); err == nil {
		return true
	}
	return false
}

func asArrayIndex(s string) int {
	i, _ := strconv.Atoi(s)
	return i
}
