package sendkinesisevent_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	sendkinesisevent "gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/model"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/server"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/server/middlewares/renderer"
)

type MockEventSender struct {
}

func (mockEventSender *MockEventSender) SendEvent(ctx context.Context, streamName string, dataSupplier model.DataSupplier, count int) error {
	return sendEventMock(ctx, streamName, dataSupplier, count)
}

var mockEventSender = &MockEventSender{}
var sendEventMock func(ctx context.Context, streamName string, dataSupplier model.DataSupplier, count int) error

func TestSendKinesisDataUnwrapperMiddleware(t *testing.T) {
	testJson := `{
		"stream": "qa-boost-insights-stream",
		"count": 100,
		"skeleton": {
			"id": "1.incrementNum",
			"item_id": "1.incrementNum",
			"product_id": "1.incrementNum",
			"like_count": "20.randomNum",
			"click_count": "40.incrementNum",
			"impression_count": "60.incrementNum",
			"calculation_time": "2021-10-28 20:14:48.097000"
		}
	}`
	req := httptest.NewRequest(http.MethodPost, "/api/send", strings.NewReader(testJson))
	req.Header.Set("Content-Type", "application/json")

	res := httptest.NewRecorder()

	r := server.NewRouter()

	renderer := renderer.New(&renderer.Options{
		IndentJSON: true,
	}, renderer.JSON)

	sendEventMock = func(ctx context.Context, streamName string, dataSupplier model.DataSupplier, count int) error {
		firstData, _ := dataSupplier.PrepareNext()
		secondData, _ := dataSupplier.PrepareNext()
		assert.True(t, strings.Contains(firstData, `"item_id":1`), "Should contain 1")
		assert.True(t, strings.Contains(secondData, `"item_id":2`), "Should contain 2")
		return nil
	}

	sendKinesisEventRes := sendkinesisevent.NewResource(&sendkinesisevent.Options{
		Renderer: renderer,
		Service:  mockEventSender,
	})

	r.AddResources(sendKinesisEventRes)

	r.ServeHTTP(res, req)

	assert.Equal(t, 200, res.Result().StatusCode, "Non 200 return")
}
