package middleware

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/Jeffail/gabs/v2"
	eventContext "gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/context"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/model"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/utils"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/server/domain"
)

var (
	parserError = errors.New("Somethings wrong")
)

type toAnalyze struct {
	DataSkeleton json.RawMessage `json:"skeleton"`
}

type MiddlewareErrorResponse struct {
	Message string `json:"message,omitempty"`
	Success bool   `json:"success"`
}

func SendKinesisDataUnwrapperMiddleware(renderer domain.IRenderer) domain.MiddlewareInterceptor {
	return func(w http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
		var toAnalyze toAnalyze

		bodyBytes, _ := ioutil.ReadAll(req.Body)
		req.Body.Close()
		req.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
		if err := json.Unmarshal(bodyBytes, &toAnalyze); err != nil {
			RenderError(renderer, w, req, http.StatusBadRequest, fmt.Sprintf("Skeleton is malformed: %s", err.Error()))
			return
		}

		container, err := gabs.ParseJSON(toAnalyze.DataSkeleton)
		if err != nil {
			RenderError(renderer, w, req, http.StatusBadRequest, fmt.Sprintf("Skeleton is malformed: %s", err.Error()))
			return
		}

		resultJSON := model.ParsedSkeleton{
			ArrayIndexes: make([]model.JsonArr, 0),
			Types:        make(map[string]model.ValueFunc),
		}
		if err = parseContainerJSON(container, "", &resultJSON); err != nil {
			RenderError(renderer, w, req, http.StatusBadRequest, fmt.Sprintf("Skeleton is malformed: %s", err.Error()))
			return
		} else {
			next(w, req.WithContext(context.WithValue(req.Context(), eventContext.PreparedDataJSON, resultJSON)))
		}
		if len(resultJSON.Types) == 0 {
			resultTEXT, err := json.Marshal(&toAnalyze.DataSkeleton)
			if err != nil {
				RenderError(renderer, w, req, http.StatusBadRequest, fmt.Sprintf("Skeleton is malformed: %s", err.Error()))
				return
			}
			next(w, req.WithContext(context.WithValue(req.Context(), eventContext.PreparedDataTEXT, string(resultTEXT))))
		}
	}
}

func parseContainerJSON(container *gabs.Container, curPath string, output *model.ParsedSkeleton) error {

	objChildren := container.ChildrenMap()
	if len(objChildren) > 0 {
		i := -1
		for key, child := range objChildren {
			i++
			childData := child.Data()
			switch typedData := childData.(type) {
			case string: // string
				valueResolvingFunc, err := resolveString(typedData)
				if err != nil {
					return err
				}
				output.Types[formKey(curPath, key)] = valueResolvingFunc.GetValuePreparer()
			case float64: // number
				valueResolvingFunc := utils.FormConstantNumProvider(typedData)
				output.Types[formKey(curPath, key)] = valueResolvingFunc.GetValuePreparer()
			case map[string]interface{}: // object
				newPath := formKey(curPath, key)
				parseContainerJSON(container.Path(key), newPath, output)
			case []interface{}: // Array
				output.ArrayIndexes = append(output.ArrayIndexes, model.JsonArr{formKey(curPath, key), i, len(child.Children())})
				for index := range childData.([]interface{}) {
					arrayPath := formKey(formKey(curPath, key), fmt.Sprintf("%d", index))
					parseContainerJSON(container.Path(arrayPath), arrayPath, output)
				}
			default:
				return parserError
			}
		}
	} else if arrayChildren := container.Children(); len(arrayChildren) > 0 {
		var index int
		lastDotIndex := strings.LastIndex(curPath, ".")
		if lastNumDivider := lastDotIndex; lastNumDivider == -1 {
			index, _ = strconv.Atoi(curPath)
		} else {
			index, _ = strconv.Atoi(curPath[lastDotIndex+1:])
		}
		output.ArrayIndexes = append(output.ArrayIndexes, model.JsonArr{curPath, index, len(arrayChildren)})

		i := -1
		for key, child := range arrayChildren {
			i++
			childData := child.Data()
			switch typedData := childData.(type) {
			case string: // string
				valueResolvingFunc, err := resolveString(typedData)
				if err != nil {
					return err
				}
				output.Types[formKey(curPath, fmt.Sprintf("%d", key))] = valueResolvingFunc.GetValuePreparer()
			case float64: // number
				valueResolvingFunc := utils.FormConstantNumProvider(typedData)
				output.Types[formKey(curPath, fmt.Sprintf("%d", key))] = valueResolvingFunc.GetValuePreparer()
			case map[string]interface{}: // object
				newPath := formKey(curPath, fmt.Sprintf("%d", key))
				parseContainerJSON(container.Path(fmt.Sprintf("%d", key)), newPath, output)
			case []interface{}: // Array
				output.ArrayIndexes = append(output.ArrayIndexes, model.JsonArr{formKey(curPath, fmt.Sprintf("%d", key)), i, len(child.Children())})
				for index := range childData.([]interface{}) {
					arrayPath := formKey(formKey(curPath, fmt.Sprintf("%d", key)), fmt.Sprintf("%d", index))
					parseContainerJSON(container.Path(formKey(fmt.Sprintf("%d", key), fmt.Sprintf("%d", index))), arrayPath, output)
				}
			default:
				return parserError
			}
		}
	} else {
		switch typedData := container.Data().(type) {
		case string:
			valueResolvingFunc, err := resolveString(typedData)
			if err != nil {
				return err
			}
			output.Types[curPath] = valueResolvingFunc.GetValuePreparer()
		case float64:
			valueResolvingFunc := utils.FormConstantNumProvider(typedData)
			output.Types[curPath] = valueResolvingFunc.GetValuePreparer()
		case map[string]interface{}: // object
			for key, _ := range typedData {
				newPath := formKey(curPath, key)
				parseContainerJSON(container.Path(newPath), newPath, output)
			}
		case []interface{}: // Array
			for index := range typedData {
				arrayPath := formKey(curPath, fmt.Sprintf("%d", index))
				parseContainerJSON(container.Path(arrayPath), arrayPath, output)
			}
		}
	}
	return nil
}

func resolveString(str string) (model.ValueResolvingFunc, error) {
	incrementNumType := utils.NumIncrementParser{Target: str}
	if incrementNumType.IsNumIncrementPrototype() {
		numPart, err := incrementNumType.GetNum()
		if err != nil {
			return nil, err
		}
		valueResolvingFunc := utils.FormIncrementNumProvider(numPart)
		return valueResolvingFunc, nil
	}

	randomNumType := utils.RandomNumParser{Target: str}
	if randomNumType.IsNumIncrementPrototype() {
		numPart, err := randomNumType.GetNum()
		if err != nil {
			return nil, err
		}
		valueResolvingFunc := utils.FormNumRandomProvider(numPart)
		return valueResolvingFunc, nil
	}

	randomStringType := utils.RandomStringParser{Target: str}
	if randomStringType.IsStringPrototype() {
		textPart := randomStringType.GetText()
		valueResolvingFunc := utils.FormRandomStringProvider(textPart)
		return valueResolvingFunc, nil
	}

	valueResolvingFunc := utils.FormConstantInterfaceProvider(str)
	return valueResolvingFunc, nil
}

func formKey(basePath, curPath string) string {
	if len(basePath) == 0 {
		return fmt.Sprintf("%s", curPath)
	}
	return fmt.Sprintf("%s.%s", basePath, curPath)
}

func RenderError(renderer domain.IRenderer, w http.ResponseWriter, req *http.Request, status int, message string) {
	renderer.Render(w, req, status, MiddlewareErrorResponse{
		Message: message,
		Success: false,
	})
}
