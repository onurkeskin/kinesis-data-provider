package middleware_test

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/context"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/middleware"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/server/middlewares/renderer"
)

func TestSendKinesisDataUnwrapperMiddleware(t *testing.T) {
	testJson := `{
		"stream": "qa-boost-insights-stream",
		"count": 100,
		"skeleton": {
			"id": "1.incrementNum",
			"item_id": "1.incrementNum",
			"product_id": "1.incrementNum",
			"like_count": "20.randomNum",
			"click_count": "40.incrementNum",
			"impression_count": "60.incrementNum",
			"calculation_time": "2021-10-28 20:14:48.097000"
		}
	}`
	req := httptest.NewRequest(http.MethodPost, "http://test.com", strings.NewReader(testJson))
	res := httptest.NewRecorder()

	renderer := renderer.New(&renderer.Options{
		IndentJSON: true,
	}, renderer.JSON)

	middleware := middleware.SendKinesisDataUnwrapperMiddleware(renderer)

	testHandler := func(w http.ResponseWriter, r *http.Request) {
		preparedJson, ok := context.GetPreparedJSONDataCtx(r.Context())

		assert.True(t, ok, "Shoudlnt have an error when preparing json")

		firstData, _ := preparedJson.PrepareNext()
		secondData, _ := preparedJson.PrepareNext()
		assert.True(t, strings.Contains(firstData, `"item_id":1`), "Should contain 1")
		assert.True(t, strings.Contains(secondData, `"item_id":2`), "Should contain 2")
	}

	middleware(res, req, testHandler)
}

func TestArray(t *testing.T) {
	testJson := `{
    "stream": "qa-seller-badge-stream",
    "count": 1,
    "skeleton":
        {
            "memberId": 33218391,
			"deneme": {"asd": "qwe"}
            "badges": [
            {
                "type": "TRUSTED"
            },
            {
                "type": "ORDER_TO_CARGO",
                "value": 32
            }
            ]
        }
	}
	`
	req := httptest.NewRequest(http.MethodPost, "http://test.com", strings.NewReader(testJson))
	res := httptest.NewRecorder()

	renderer := renderer.New(&renderer.Options{
		IndentJSON: true,
	}, renderer.JSON)

	middleware := middleware.SendKinesisDataUnwrapperMiddleware(renderer)

	testHandler := func(w http.ResponseWriter, r *http.Request) {
		preparedJson, ok := context.GetPreparedJSONDataCtx(r.Context())

		assert.True(t, ok, "Shoudlnt have an error when preparing json")

		firstData, _ := preparedJson.PrepareNext()
		secondData, _ := preparedJson.PrepareNext()
		assert.True(t, strings.Contains(firstData, `"type": "TRUSTED"`), "Should contain trusted")
		assert.True(t, strings.Contains(secondData, `"type": "ORDER_TO_CARGO"`), "Should contain order to cargo")
		assert.True(t, strings.Contains(secondData, `"value": 32`), "Should contain 32")
	}

	middleware(res, req, testHandler)
}
