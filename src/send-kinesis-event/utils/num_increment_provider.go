package utils

import (
	"regexp"
	"strconv"

	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/model"
)

var (
	NUM_INCREMENT_REGEXP, _  = regexp.Compile("([-.0-9]+)(.incrementNum)")
	num_increment_text_group = 1
)

type NumIncrementParser struct {
	Target      string
	cachedSlice []string
}

func (numIncrementParser *NumIncrementParser) IsNumIncrementPrototype() bool {
	if numIncrementParser.cachedSlice == nil {
		numIncrementParser.cachedSlice = NUM_INCREMENT_REGEXP.FindStringSubmatch(numIncrementParser.Target)
	}

	return len(numIncrementParser.cachedSlice) > 0
}

func (numIncrementParser *NumIncrementParser) GetNum() (float64, error) {
	if numIncrementParser.cachedSlice == nil {
		numIncrementParser.cachedSlice = NUM_INCREMENT_REGEXP.FindStringSubmatch(numIncrementParser.Target)
	}

	return strconv.ParseFloat(numIncrementParser.cachedSlice[num_increment_text_group], 64)
}

func FormIncrementNumProvider(start float64) model.ValueResolvingFunc {
	return func() model.ValueFunc {
		a := start
		return func() interface{} {
			curVal := a
			a = a + 1
			return curVal
		}
	}
}
