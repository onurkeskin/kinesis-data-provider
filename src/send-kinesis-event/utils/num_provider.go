package utils

import "gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/model"

func FormConstantNumProvider(num float64) model.ValueResolvingFunc {
	return func() model.ValueFunc {
		return func() interface{} {
			return num
		}
	}
}
