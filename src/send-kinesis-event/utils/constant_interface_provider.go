package utils

import "gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/model"

func FormConstantInterfaceProvider(i interface{}) model.ValueResolvingFunc {
	return func() model.ValueFunc {
		return func() interface{} {
			return i
		}
	}
}
