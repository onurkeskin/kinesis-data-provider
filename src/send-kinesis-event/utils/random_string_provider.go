package utils

import (
	"fmt"
	"math/rand"
	"regexp"

	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/model"
)

const (
	stringLength = 10
)

var (
	RANDOM_STRING_REGEXP, _      = regexp.Compile(`(^(\w|\s)+)(.randomString)`)
	random_string_text_group int = 1
)

type RandomStringParser struct {
	Target      string
	cachedSlice []string
}

func (randomStringRegexp *RandomStringParser) IsStringPrototype() bool {
	if randomStringRegexp.cachedSlice == nil {
		randomStringRegexp.cachedSlice = RANDOM_STRING_REGEXP.FindStringSubmatch(randomStringRegexp.Target)
	}

	return len(randomStringRegexp.cachedSlice) > 0
}

func (randomStringRegexp *RandomStringParser) GetText() string {
	if randomStringRegexp.cachedSlice == nil {
		randomStringRegexp.cachedSlice = RANDOM_STRING_REGEXP.FindStringSubmatch(randomStringRegexp.Target)
	}

	return randomStringRegexp.cachedSlice[random_string_text_group]
}

func FindRandomStringPrototype(str string) []string {
	return RANDOM_STRING_REGEXP.FindStringSubmatch(str)
}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func FormRandomStringProvider(start string) model.ValueResolvingFunc {
	return func() model.ValueFunc {
		return func() interface{} {
			return fmt.Sprintf("%s%s", start, randSeq(stringLength))
		}
	}
}

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
