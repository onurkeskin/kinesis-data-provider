package utils

import (
	"math/rand"
	"regexp"
	"strconv"
	"time"

	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/model"
)

const (
	RANDOM_NUM_CAP = 10000
)

var (
	RANDOM_NUM_REGEXP, _  = regexp.Compile("([-.0-9]+)(.randomNum)")
	random_num_text_group = 1
)

type RandomNumParser struct {
	Target      string
	cachedSlice []string
}

func (randomNumParser *RandomNumParser) IsNumIncrementPrototype() bool {
	if randomNumParser.cachedSlice == nil {
		randomNumParser.cachedSlice = RANDOM_NUM_REGEXP.FindStringSubmatch(randomNumParser.Target)
	}

	return len(randomNumParser.cachedSlice) > 0
}

func (randomNumParser *RandomNumParser) GetNum() (float64, error) {
	if randomNumParser.cachedSlice == nil {
		randomNumParser.cachedSlice = RANDOM_NUM_REGEXP.FindStringSubmatch(randomNumParser.Target)
	}

	return strconv.ParseFloat(randomNumParser.cachedSlice[num_increment_text_group], 64)
}

func FormNumRandomProvider(start float64) model.ValueResolvingFunc {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	return func() model.ValueFunc {
		return func() interface{} {
			return start + float64(r1.Intn(RANDOM_NUM_CAP))
		}
	}
}
