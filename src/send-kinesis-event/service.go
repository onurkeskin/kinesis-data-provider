package sendkinesisevent

import (
	"context"
	"math"

	log "github.com/sirupsen/logrus"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/model"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/kinesis"
	"github.com/aws/aws-sdk-go-v2/service/kinesis/types"
)

const (
	defaultPartitionKey           = "Key"
	KINESIS_MAX_BATCH_COUNT int64 = 500
)

type EventSender interface {
	SendEvent(ctx context.Context, streamName string, dataSupplier model.DataSupplier, count int) error
}

func NewSendKinesisEventService(region string) *SendKinesisEventService {
	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		log.WithFields(log.Fields{"Error": err.Error()}).Error("Unable to load aws config")
	}

	return &SendKinesisEventService{
		cfg: cfg,
		client: kinesis.New(kinesis.Options{
			Region:      region,
			Credentials: cfg.Credentials,
		}),
	}
}

type SendKinesisEventService struct {
	cfg    aws.Config
	client *kinesis.Client
}

func (sendKinesisEventService *SendKinesisEventService) SendEvent(ctx context.Context, streamName string, dataSupplier model.DataSupplier, count int) error {
	svc := sendKinesisEventService.client

	var batches int64 = int64(math.Ceil(float64(count) / float64(KINESIS_MAX_BATCH_COUNT)))

	batchRecords := make([]types.PutRecordsRequestEntry, 0)
	for batch := int64(1); batch <= batches; batch++ {
		for cur := int64(0); cur < int64(count)%(batch*KINESIS_MAX_BATCH_COUNT); cur++ {
			nextData, err := dataSupplier.PrepareNext()
			if err != nil {
				return err
			}
			batchRecords = append(batchRecords, types.PutRecordsRequestEntry{
				Data:         []byte(nextData),
				PartitionKey: aws.String(defaultPartitionKey),
			})
		}

		params := &kinesis.PutRecordsInput{
			Records:    batchRecords,
			StreamName: aws.String(streamName),
		}

		_, err := svc.PutRecords(ctx, params)
		if err != nil {
			log.WithFields(log.Fields{"Error": err.Error()}).Error("Error recieved from aws")
		}

		batchRecords = nil
	}

	return nil
}
