package context

import (
	"context"

	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/model"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/server/domain"
)

const (
	PreparedDataJSON domain.ContextKey = "preparedDataJSON"
	PreparedDataTEXT domain.ContextKey = "preparedDataTEXT"
)

func GetPreparedJSONDataCtx(ctx context.Context) (model.ParsedSkeleton, bool) {
	skeleton, ok := ctx.Value(PreparedDataJSON).(model.ParsedSkeleton)
	return skeleton, ok
}

func GetPreparedTEXTDataCtx(ctx context.Context) (string, bool) {
	text, ok := ctx.Value(PreparedDataTEXT).(string)
	return text, ok
}
