package sendkinesisevent

import (
	"encoding/json"
	"fmt"
	"net/http"

	kinesisContext "gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/context"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/model"
)

type SendKinesisDataRequest_v0 struct {
	Stream string `json:"stream"`
	Count  int32  `json:"count"`
}

type SendKinesisDataResponse_v0 struct {
	Message string `json:"message,omitempty"`
	Success bool   `json:"success"`
}

type ErrorResponse_v0 struct {
	Message string `json:"message,omitempty"`
	Success bool   `json:"success"`
}

func (resource *Resource) RenderError(w http.ResponseWriter, req *http.Request, status int, message string) {
	resource.Render(w, req, status, ErrorResponse_v0{
		Message: message,
		Success: false,
	})
}

func (resource *Resource) HandleSendKinesisRequest_v0(w http.ResponseWriter, req *http.Request) {
	body := SendKinesisDataRequest_v0{}
	if err := resource.DecodeRequestBody(w, req, &body); err != nil {
		resource.RenderError(w, req, http.StatusBadRequest, err.Error())
		return
	}

	if data, ok := kinesisContext.GetPreparedJSONDataCtx(req.Context()); !ok {
		resource.RenderError(w, req, http.StatusBadRequest, "Something went wrong when pushing to kinesis")
	} else {
		err := resource.Service.SendEvent(req.Context(), body.Stream, &data, int(body.Count))
		if err != nil {
			resource.RenderError(w, req, http.StatusBadRequest, fmt.Sprintf("Something went wrong when pushing to kinesis: %v", err.Error()))
			return
		}
		resource.Render(w, req, http.StatusOK, SendKinesisDataResponse_v0{
			Success: true,
			Message: "JSON Data Sent",
		})
		return
	}

	if data, ok := kinesisContext.GetPreparedTEXTDataCtx(req.Context()); !ok {
		err := resource.Service.SendEvent(req.Context(), body.Stream, model.TextData(data), int(body.Count))
		if err != nil {
			resource.RenderError(w, req, http.StatusBadRequest, fmt.Sprintf("Something went wrong when pushing to kinesis: %v", err.Error()))
			return
		}

		resource.Render(w, req, http.StatusCreated, SendKinesisDataResponse_v0{
			Success: true,
			Message: "TEXT Data Sent",
		})
		return
	}
}

func (resource *Resource) DecodeRequestBody(w http.ResponseWriter, req *http.Request, target interface{}) error {
	decoder := json.NewDecoder(req.Body)
	return decoder.Decode(target)
}
