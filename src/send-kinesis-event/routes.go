package sendkinesisevent

import (
	"strings"

	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/send-kinesis-event/middleware"
	"gitlab.trendyol.com/dolap/common/test-utils/kinesis-test-data-producer/src/server/domain"
)

const (
	SendKinesisEvent = "SendKinesisEvent"
)

const defaultBasePath = "/api/"

func (resource *Resource) generateRoutes(basePath string) *domain.Routes {
	if basePath == "" {
		basePath = defaultBasePath
	}
	var baseRoutes = domain.Routes{

		domain.Route{
			Name:           SendKinesisEvent,
			Method:         "POST",
			Pattern:        "/api/send",
			DefaultVersion: "0.0",
			RouteHandlers: domain.RouteHandlers{
				"0.0": domain.HandlerFunc{
					Controller: resource.HandleSendKinesisRequest_v0,
					Middleware: domain.MiddlewareChain{middleware.SendKinesisDataUnwrapperMiddleware(resource.Renderer)}},
			},
		},
	}

	routes := domain.Routes{}

	for _, route := range baseRoutes {
		r := domain.Route{
			Name:           route.Name,
			Method:         route.Method,
			Pattern:        strings.Replace(route.Pattern, defaultBasePath, basePath, -1),
			DefaultVersion: route.DefaultVersion,
			RouteHandlers:  route.RouteHandlers,
		}
		routes = routes.Append(&domain.Routes{r})
	}
	resource.routes = &routes
	return resource.routes
}
